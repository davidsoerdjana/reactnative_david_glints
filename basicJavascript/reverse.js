function reverseMyName(str) {
    return str.split("").reverse().join("");
}

// reverseMyName("hello");
console.log(reverseMyName("A"));
console.log(reverseMyName("Michael Jackson"));
console.log(reverseMyName("Alvian Zachry Faturrahman"));
console.log(reverseMyName(","));


const Test = (fun, result) => console.log(reverseMyName(fun) === result)

// Test("A", "A")
// Test("Michael Jackson","noskcaJ leahciM")
// Test("Alvian Zachry Faturrahman", "namharrutaF yrhcaZ naivlA")
// Test("", "")