// function diskon(x) {
//     let musimPandemik = (x * 30) / 100
//     return musimPandemik
// }
// let sale = diskon(20000)
// console.log(sale)


// function sayHiTo(name) {
//     let halo = `Hai ${name.toUpperCase()}!`
//     return halo
// }

// let test1 = sayHiTo("everything")
// console.log(test1)
// // Output: Hai EVERYTHING!

// let test2 = sayHiTo(100) // Bukan string, output akan error
// console.log(test2)
// // Output: TypeError: name.toUpperCase is not a function


// // Function Declaration (ES5)
// function dmLingkaran(r) {
//     return 3.14 * r * r
// }
// console.log('Luas Lingkaran:', dmLingkaran(10))

// function dmLingkar(r) {
//     return 3.14 * 2 * r
// }
// console.log('Keliling Lingkaran:', dmLingkar(10))


// // Function Expression
// const dLingkaran = function (r) {
//     return 3.14 * r * r
// }
// console.log('Luas Lingkaran:', dLingkaran(10))

// const dLingkar = function (r) {
//     return 3.14 * 2 * r
// }
// console.log('Keliling Lingkaran:', dLingkar(10))


// // Arrow Function (ES6)
// const diameter = (r) => 3.14 * r * r
// console.log('Luas Lingkaran:', diameter(10))

// const diametr = (r) => 3.14 * 2 * r
// console.log('Keliling Lingkaran:', diametr(10))


// // Sebuah fungsi juga bisa menerima fungsi sebagai parameternya, yang dikenal dengan istilah Higher Order Function
// const strArray = ['JavaScript', 'Java', 'C'];

// function forEach(array, callback) {
//     const newArray = [];
//     for (let i = 0; i < array.length; i++) {
//         newArray.push(callback(array[i]));
//     }
//     return newArray;
// }
// const lenArray = forEach(strArray, (item) => {
//     return item.length;
// });
// console.log(lenArray);
// // Output: [ 10, 4, 1 ]


// // persegi
// // luas = s * s
// // keliling = 4 * s

// // persegi panjang
// // luas = p * l
// // keliling = 2 * (p + l)

// // jajar genjang (a=alas, b=sisi miring, t=tinggi)
// // luas = a * t
// // keliling = 2 * (a + b)

// // segitiga
// // luas = (a * t) / 2
// // keliling = a + b + c

// class BangunDatar {
//     constructor(s, p, l, a, t, b, c) {
//         this.s = s;
//         this.p = p;
//         this.l = l;
//         this.a = a;
//         this.t = t;
//         this.b = b;
//         this.c = c;
//     }

//     // //arrow function
//     // luasPersegi = (s) => s * s

//     luasPersegi(s) {
//         return s * s;
//     }

//     kelilingPersegi(s) {
//         return 4 * s;
//     }

//     luasPersegiPanjang(p, l) {
//         return p * l;
//     }

//     kelilingPersegiPanjang(p, l) {
//         return 2 * (p + l);
//     }

//     luasJajarGenjang(a, t) {
//         return a * t;
//     }

//     kelilingJajarGenjang(a, b) {
//         return 2 * (a + b);
//     }

//     luasSegitiga(a, t) {
//         return (a * t) / 2;
//     }

//     kelilingSegitiga(a, b, c) {
//         return a + b + c;
//     }
// };

// let bangunDatar = new BangunDatar;

// console.log('Keliling Persegi: '+ bangunDatar.kelilingPersegi(5));
// console.log('Luas Persegi: '+ bangunDatar.luasPersegi(5));
// console.log("---------------------");

// console.log('Keliling Persegi Panjang: '+ bangunDatar.kelilingPersegiPanjang(5, 4));
// console.log('Luas Persegi Panjang: '+ bangunDatar.luasPersegiPanjang(5, 4));
// console.log("---------------------");

// console.log('Keliling Jajar Genjang: '+ bangunDatar.kelilingJajarGenjang(5, 3));
// console.log('Luas Jajar Genjang: '+ bangunDatar.luasJajarGenjang(5, 4));
// console.log("---------------------");

// console.log('Keliling Segitiga: '+ bangunDatar.kelilingSegitiga(5, 2, 3));
// console.log('Luas Segitiga: '+ bangunDatar.luasSegitiga(5, 4));
// console.log("---------------------");

// // inheritance
// class bangunDatar {
//     constructor() {
//         if (this.constructor === bangunDatar) {
//             throw new Error("Cannot instantiate from Abstract Class")
//             // Because it's abstract
//         }
//         this.nama = this.constructor.name;
//     }

//     getName() {
//         console.log(`Nama bangun datar ini ${this.nama}`)
//     }
// }

// class Persegi extends bangunDatar {
//     constructor(sisi) {
//         super();
//         this.sisi = sisi;
//     }

//     keliling() {
//         return 4 * this.sisi;
//     }

//     luas() {
//         return this.sisi * this.sisi;
//     }
// }

// class PersegiPanjang extends bangunDatar {
//     constructor(panjang, lebar) {
//         super();
//         this.panjang = panjang;
//         this.lebar = lebar;
//     }

//     keliling() {
//         return 2 * (this.panjang + this.lebar);
//     }

//     luas() {
//         return this.panjang * this.lebar;
//     }
// }

// class jajarGenjang extends bangunDatar {
//     constructor(sisi_bawah, sisi_atas, tinggi) {
//         super();
//         this.sisi_bawah = sisi_bawah;
//         this.sisi_atas = sisi_atas;
//         this.tinggi = tinggi;
//     }

//     keliling() {
//         return 2 * (this.sisi_bawah + this.sisi_atas);
//     }

//     luas() {
//         return this.sisi_bawah * this.tinggi;
//     }
// }

// // Instansiasi objek
// const persegiku = new Persegi(2);
// const persegiPanjangku = new PersegiPanjang(2, 4);
// const jajar = new jajarGenjang(3, 3, 4);
// const jajar2 = new jajarGenjang(5, 5, 10);

// persegiku.getName();
// console.log("Keliling : " + persegiku.keliling());
// console.log("Luas : " + persegiku.luas());

// persegiPanjangku.getName();
// console.log("Keliling : " + persegiPanjangku.keliling());
// console.log("Luas : " + persegiPanjangku.luas());

// jajar.getName();
// console.log("Keliling : " + jajar.keliling());
// console.log("Luas : " + jajar.luas());

// jajar2.getName();
// console.log("Keliling : " + jajar2.keliling());
// console.log("Luas : " + jajar2.luas());


// class Human {
//     static isLivingOnEarth = true; // Add static property
//     constructor(name, address) { // Add constructor method
//         this.name = name;
//         this.address = address;
//     }
//     introduce() { // Add instance method signature
//         console.log(`Hi, my name is ${this.name}`)
//     }
// }
// console.log(Human.isLivingOnEarth) // Output static property: true

// Human.prototype.greet = function (name) { // Add prototype/instance method
//     console.log(`Hi, ${name}, I'm ${this.name}`)
// }

// Human.destroy = function (thing) { // Add static method
//     console.log(`Human is destroying ${thing}`)
// }

// let mj = new Human("Michael Jackson", "Isekai"); // Instantiation of Human class, we create a new object
// console.log(mj); // Output: Human {name: "Michael Jackson", address: "Isekai"}
// console.log(mj instanceof Human) // true. Checking instance of class
// console.log(mj.introduce()) // Hi, my name is Michael Jackson
// console.log(mj.greet("Donald Trump")); // Hi, Donald Trump, I'm Michael Jackson
// console.log(Human.destroy("Amazon Forest")); // Human is destroying Amazon Forest


// // 4 pilar atau konsep OOP antara lain: Inheritance, Encapsulation, Abstraction, Polymorphism
// // Inheritance/pewarisan
// class Human {
//     constructor(name, address) {
//         this.name = name;
//         this.address = address;
//     }
//     introduce() {
//         console.log(`Hi, my name is ${this.name}`)
//     }
//     work() {
//         console.log("Work!")
//     }
// }

// class Programmer extends Human { // Create a child class from Human
//     constructor(name, address, programmingLanguages) {
//         super(name, address) // Call the super/parent class constructor
//         this.programmingLanguages = programmingLanguages;
//     }
//     introduce() {
//         super.introduce();
//         console.log(`I can write `, this.programmingLanguages); // Call the super class introduce instance method
//     }
//     code() {
//         console.log(
//             "Code some",
//             this.programmingLanguages[
//                 Math.floor(Math.random() * this.programmingLanguages.length)
//             ]
//         )
//     }
// }

// // Initiate from Human directly. kita coba membuat objek baru dari kedua class tersebut dan menggunakan method yang tersedia di class tersebut
// let Obama = new Human("Barrack Obama", "Washington DC");
// Obama.introduce() // Hi, my name is Barack Obama
// let Isyana = new Programmer("Isyana", "Jakarta", ["Javascript", "Kotlin", "Python"]);
// Isyana.introduce() // Hi, my name is Isyana; I can write ["Javascript", "Kotlin", "Python"]
// Isyana.code() // Code some Javascript/Ruby/...
// Isyana.work() // Call super class method that isn't overrided or overloaded
// try {
//     Obama.code() // Error: Undefined method "code". Obama can't code since Obama is an direct instance of Human, which don't have code method
// } catch (err) {
//     console.log(err.message)
// }
// console.log(Isyana instanceof Human) // true
// console.log(Isyana instanceof Programmer) // true


// // Ada dua method untuk konsep inheritance, overriding method & overloading method
// /*Overriding method. mengubah method dari super class untuk diimplementasi
// ulang di dalam sub classnya, berarti gak mengubah parameter yang sudah didefinisikan
// oleh super classnya*/
// class Person {
//     constructor(name, address) {
//         this.name = name;
//         this.address = address;
//     }
//     introduce() {
//         console.log(`Hi, my name is ${this.name}`)
//     }
// }

// class Programmer extends Person { // Create a child class from Person
//     constructor(name, address, programmingLanguages) {
//         super(name, address) // Call the super/parent class constructor
//         this.programmingLanguages = programmingLanguages;
//     }

//     introduce() { // Override the Introduce Method
//         super.introduce(); // Call the super class introduce instance method.
//         console.log(`I can write `, this.programmingLanguages);
//     }

//     code() {
//         console.log("Code some", this.programmingLanguages[
//                 Math.floor(Math.random() * this.programmingLanguages.length)
//             ]
//         )
//     }
// }
// let Isyana = new Programmer("Isyana Karina", "Jakarta", ["Javascript", "Python"]);
// Isyana.introduce() // Hi, my name is Isyana; I can write ["Javascript", "Python"]
// Isyana.code()


// /*Overloading method ini sama seperti overriding method. Tapi di dalam overloading ini, kita
// mengubah definisi parameter dari super class.
// Maksudnya, nama method yang kita gunakan sama dengan nama method di super classnya,
// tapi parameter yang ada di subclassnya berbeda*/
// class Person {
//     constructor(name, address) {
//         this.name = name;
//         this.address = address;
//     }
//     introduce() {
//         console.log(`Hi, my name is ${this.name}`)
//     }
// }

// class Programmer extends Person { // Create a child class from Person
//     constructor(name, address, programmingLanguages) {
//         super(name, address) // Call the super/parent class constructor
//         this.programmingLanguages = programmingLanguages;
//     }

//     introduce(withDetail) { // Overload the Introduce Method
//         super.introduce(); // Call the super class introduce instance method
//         (Array.isArray(withDetail)) ?
//         console.log(`I can write ${this.programmingLanguages}`): console.log("Wrong input")
//     }
//     code() {
//         let acak = Math.floor(Math.random() * this.programmingLanguages.length)
//         console.log("Code some", this.programmingLanguages[acak])
//     }
// }
// let Isyana = new Programmer("Isyana Karina", "Jakarta", ["JavaScript", " Kotlin"]);
// Isyana.introduce(["JavaScript"])
// // Hi, my name is Isyana; I can write ...
// //Isyana.introduce("JavaScript") // Hi, my name is Isyana; Wrong Input
// //Isyana.introduce(1) // Hi, my name is Isyana; Wrong Input
// Isyana.code() //Code some ...


/*Encapsulation(pembungkusan)
Data kita bisa disembunyikan dengan suatu cara yang dinamakan visibility, yang bertujuan biar method
dan variable gak bisa diakses secara langsung dari luar class. Jenis visibility yang dimaksud ada 3 macam: public, private, and protected.
Public. Suatu visibility level, dimana bila kita mendefinisikan suatu method atau property secara publik,
artinya method / property itu bisa dipanggil di luar deklarasi kelas*/
// class Human {
//     constructor(name, address) {
//         this.name = name;
//         this.address = address;
//     }

//     introduce() { // This is public instance method
//         console.log(`Hello, my name is ${this.name}`)
//     }

//     static isEating(food) { // This is public static method
//         let foods = ["plant", "animal"];
//         return foods.includes(food.toLowerCase());
//     }
// }
// let mj = new Human("Michael Jackson", "Isekai");
// console.log(mj) // Output: Human {name: "Michael Jackson", address: "Isekai"}
// console.log(mj.introduce());
// console.log(Human.isEating("Plant")) // true
// console.log(Human.isEating("Human")) // false


/*Private. Suatu method/property yang gak bisa diakses di luar deklarasi class. Ini berarti kita gak bisa akses method/property
dari luar kurung kurawal class/scope dari kelas tersebut. Sejak ES8 hadir di JavaScript, untuk mendeklarasikan
suatu private method atau private property, kita bisa gunakan tanda pagar (#) sebelum nama class.*/
// class Human {
//     constructor(name, address) {
//         this.name = name;
//         this.address = address;

//     }#doGossip = () => {
//         console.log(`My address will become viral ${this.address}`)
//     }
//     talk() {
//         console.log(this.#doGossip()); // Call the private method
//     }
//     static #isHidingArea = true;
// }

// let mj = new Human("Michael Jackson", "Isekai");
// console.log(mj.talk()) // Will run, won't return error! Output: My address will become viral Isekai

// // try {
// //     Human.#isHidingArea // Will return an error!
// //     mj.#doGossip() // Won't run, will return error!
// // } catch (err) {
// //     console.error(err)
// // } // Private field '#isHidingArea' must be declared in an enclosing class


/*Protected. Visibility ini dapat kita akses di dalam sub class. Ini yang menjadi pembeda
antara private dengan protected. Kita bisa menambahkan tanda _ sebelum nama method / property
untuk memberi tahu bahwa itu protected untuk developer lain.*/
// class Human {
//     constructor(name, address) {
//         this.name = name;
//         this.address = address;
//     }
//     _call() {
//         console.log(`Call me as a ${this.name}`)
//     }
// }
// class Programmer extends Human {
//     constructor(name, address, task, salary) {
//         super(name, address);
//         this.task = task;
//         this.salary = salary;
//     }
//     doCall() {
//         super._call() // Will run
//     }
// }
// let sb = new Human("Sabrina", "Jakarta");
// let job = new Programmer("Developer", "$1000");
// console.log(sb._call()) // Call me as a Sabrina
// /*Meskipun ini gak error ketika kita panggil protected secara public. Tapi,
// kita harus paham method ini protected, yang semestinya hanya boleh
// dipanggil di dalam class declaration atau sub-classnya.*/
// console.log(job.doCall()) // Call me as a Developer


/*Implementasi Encapsulation untuk menyembunyikan method #encrypt dan #decrypt.
Kita gak mau ada orang lain yang menggunakan method tersebut di luar kelas deklarasi,
karena hal itu berbahaya*/
// class User {
//     constructor(props) {
//         // props is object, because it is better that way
//         let {
//             email,
//             password
//         } = props; // Destruct
//         this.email = email;
//         this.encryptedPassword = this.#encrypt(password);
//         // We won't save the plain password
//     }
//     // Private method
//     #encrypt = (password) => {
//         return `encrypted-version-of-${password}`
//     }
//     // Getter
//     #decrypt = () => {
//         return this.encryptedPassword.split(`encrypted-version-of-`)[1];
//     }
//     authenticate(password) {
//         return this.#decrypt() === password; // Will return true or false
//     }
// }
// let Bot = new User({
//     email: "bot@mail.com",
//     password: "123456"
// })
// const isAuthenticated = Bot.authenticate("123456");
// console.log(isAuthenticated) // true


// // Abstract
// class Human {
//     constructor(props) {
//         if (this.constructor === Human) {
//             throw new Error("Cannot instantiate from Abstract Class") // Because it's abstract
//         }
//         let {
//             name,
//             address
//         } = props;
//         this.name = name; // Every human has name
//         this.address = address; // Every human has address
//         this.profession = this.constructor.name; // Every human has profession, and let the child class to define it
//     }
//     work() { // Yes, every human can work
//         console.log("Working...")
//     }
//     introduce() { // Every human can introduce
//         console.log(`Hello, my name is ${name}`)
//     }
// }

// class Police extends Human {
//     constructor(props) {
//         super(props);
//         this.rank = props.rank; // Add new property, rank.
//     }
//     work() {
//         console.log("Go to the police station");
//         super.work();
//     }
// }
// const Wiranto = new Police({
//     name: "Wiranto",
//     address: "Unknown",
//     rank: "General"
// })
// console.log(Wiranto.profession); // Police

// // Jika kita coba melakukan instansiasi class Human, yang mana sudah ada kondisi untuk abstraction,
// // maka kita akan dapat pesan error
// try { 
//     let Abstract = new Human({
//         name: "Abstract",
//         address: "Unknown"
//     })
// } catch (err) {
//     console.log(err.message)
// } // Cannot instantiate from Abstract Class


/*Polymorphism. Konsep ini memiliki arti bahwa satu class dapat memiliki banyak wujud dari sub classnya.
Biasanya sub classnya memiliki perilaku yang sangat berbeda dari super classnya.
Prinsip ini berlaku ketika kita punya banyak class yang terkait satu sama lain melalui inheritance*/
// class Human {
//     constructor(name, address) {
//         this.name = name;
//         this.address = address;
//     }
//     introduce() {
//         console.log(`Hi, my name is ${this.name}`)
//     }
//     work() {
//         console.log(`${this.constructor .name}:`, "Working!")
//     }
// }

// /*Untuk implementasi module / helper ini kita menggunakan konsep mix-ins. Mix-ins atau abstract subclasses ini ibarat suatu
// template untuk class. Kita pakai konsep mix-ins ini karena class di ECMAScript hanya dapat
// memiliki satu superclass. Sedangkan kita butuh fungsi dengan superclass sebagai input dan subclass yang memperluas superclass sebagai output*/
// const PublicServer = Base => class extends Base { // Public Server Module/Helper
//     save() {
//         console.log("People say Thank You!")
//     }
// }

// const Military = Base => class extends Base { // Military Module/Helper
//     shoot() {
//         console.log("DOR!")
//     }
// }

// class Doctor extends PublicServer(Human) {
//     constructor(props) {
//         super(props);
//     }
//     work() {
//         super.work(); // From Human Class
//         super.save(); // From Public Server Class
//     }
// }
// class Police extends PublicServer(Military(Human)) {
//     static workplace = "Police Station";
//     constructor(props) {
//         super(props);
//         this.rank = props.rank;
//     }
//     work() {
//         super.work();
//         super.shoot(); // From Military class
//         super.save(); // From Public Server Class
//     }
// }

// class Army extends PublicServer(Military(Human)) {
//     static workplace = "Police Station";
//     constructor(props) {
//         super(props);
//         this.rank = props.rank;
//     }
//     work() {
//         super.work();
//         super.shoot(); // From Military class
//         super.save(); // From Public Server Class
//     }
// }
// class Writer extends Human {
//     work() {
//         console.log("Write books");
//         super.work();
//     }
// }   

// const Wiranto = new Police({ // Instantiate Military Based Class
//     name: "Wiranto",
//     address: "Unknown",
//     rank: "General"
// })
// const Prabowo = new Army({
//     name: "Prabowo",
//     address: "Undefined",
//     rank: "General"
// })

// const Boyke = new Doctor({ // Instantiate Doctor
//     name: "Boyke",
//     address: "Jakarta"
// })

// const Dee = new Writer({ // Instantiate Writer
//     name: "Dee",
//     address: "Bandung"
// })

// console.log("Wiranto shoot: "), Wiranto.shoot(); // DOR!
// console.log("--------------");
// console.log("Prabowo shoot: "), Prabowo.shoot(); // DOR!
// console.log("--------------");
// console.log("Wiranto save: "), Wiranto.save() // People say Thank You!
// console.log("--------------");
// console.log("Prabowo save: "), Prabowo.save() // People say Thank You!
// console.log("--------------");
// console.log("Boyke save: "), Boyke.save() // People say Thank You!
// console.log("--------------");
// console.log("Wiranto work: "), Wiranto.work() // Police: Working! DOR! People say Thank You!
// console.log("--------------");
// console.log("Prabowo work: "), Prabowo.work() // Army: Working! DOR! People say Thank You!
// console.log("--------------");
// console.log("Boyke work: "), Boyke.work() // Doctor: Working! People say Thank You!
// console.log("--------------");
// console.log("Dee work: "), Dee.work() // Write books. Writer: Working!


// // Contoh lain Polymorphism
class Electronic{
    constructor(name){
        this.name = name;
    }

    setName(){
        console.log(this.name+ " adalah barang electronic");
    }
}

class David extends Electronic{
    constructor(name){
        super(name);
    }

    setName(){
        console.log(this.name + " bukan barang elektronik");
    }
}

let komputer = new Electronic("Komputer");
komputer.setName();

let komputer1 = new David("David");
komputer1.setName();