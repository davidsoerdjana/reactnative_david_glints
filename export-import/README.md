Install Prompt-sync(to run prompt di VScode like run in browser):
npm install prompt-sync

---

Install Babel for Javascript ES6(to running "export-import" folder):

1. npm init
   (just enter and enter until finish)
2. npm install --save-dev @babel/core @babel/cli @babel/preset-env @babel/node
   or
   yarn add @babel/cli @babel/core @babel/node @babel/preset-env
3. to run project export-import, get into export-import folder then: npx babel-node main.js
