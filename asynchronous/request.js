const request = require('request');

const options = {
  // URL tergantung dokumentasi backend API
  url: 'https://api.github.com/repos/request/request',
  headers: {
    'User-Agent': 'request'
  },
  // json: true // Automatically parses the JSON string in the response
};

function callback(error, response, body) {
  if (!error && response.statusCode == 200) {
    const info = JSON.parse(body);
    console.log(body.name);
    console.log(body.stargazers_count + " Stars");
    console.log(body.forks_count + " Forks");
  }
}
request(options, callback);