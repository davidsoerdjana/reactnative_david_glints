// const UserProfile = function (id, callback) { // penamaan parameter bebas
//   if (!id) {
//     return callback(new Error('Invalid userId'))
//   }
//   let result = {
//     success: true,
//     id: id,
//     message: 'User Found'
//   }
//   return callback(null, result)
// }

// UserProfile(10, function (err, result) { // parameter pertama(err) untuk error, parameter kedua(result) utk datanya
//   if (err) {
//     console.log(err.message)
//   }
//   console.log(result)
// })

// -------------------------------------

const UserProfile = function (id, callback) {
  if (!id) {
    return callback(new Error('Invalid UserID'));
  }

  let result = {
    success: true,
    id: id,
    message: 'User Found'
  }
  // let result;

  if (result) {
    return callback(null, result);
  } else {
    return callback(new Error('Datanya nggak ada'));
  }
}

const UserArticle = function (id, callback) {
  if (!id) {
    return callback(new Error('Invalid UserID'))
  }

  let result = {
    success: true,
    id: id,
    message: 'Article Found',
    data: [],
  }
  // let result;

  if (result) {
    return callback(null, result);
  } else {
    return callback(new Error('Datanya nggak ada'));
  }
}

UserProfile("", function (err, result) {
  if (err) {
    console.log(err);
  }
  console.log(result);
})

UserProfile("saya", function (err, result) {
  if (err) {
    console.info(err);
  } else {
    console.info(result);

    UserArticle("saya", function (err, result) {
      if (err) {
        console.info(err);
      } else {
        console.info(result);
      }
    })
  }
})