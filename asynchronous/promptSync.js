// // Working with input
// const readline = require("readline").createInterface({
//   input: process.stdin,
//   output: process.stdout,
// });

// readline.question("Who are you?", (name) => {
//   console.log(`Hey there ${name}!`);
//   readline.close();
// });

// -----------------------------------------

// // Making a basic app
// const prompt = require('prompt-sync')(); //({ sigint: true });

// const numberToGuess = Math.floor(Math.random() * 10) + 1; // Random number from 1 - 10
// let foundCorrectNumber = false; // This variable is used to determine if the app should continue prompting the user for input

// while (!foundCorrectNumber) {
//   let guess = prompt("Guess a number from 1 to 10: "); // Get user input
//   guess = Number(guess); // Convert the string input to a number

//   if (guess === numberToGuess) {
//     // Compare the guess to the secret answer and let the user know.
//     console.log("Congrats, you got it!");
//     foundCorrectNumber = true;
//   } else {
//     console.log("Sorry, guess again!");
//   }
// }

// -------------------------------------------

// // All user input will be read as a String, so in order to treat user input as numbers,
// // you’ll need to convert the input:
// const prompt = require('prompt-sync')();

// const num = prompt("Enter a number: ");
// console.log("Your number + 4 =");
// console.log(Number(num) + 4);

// -----------------------------------------

// const prompt = require('prompt-sync')();

// const name = prompt('What is your name?');
// console.log(`Hey there ${name}`);