import React from 'react';
import axios from 'axios';
import './App.css';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listNews: [],
    }
  }

  componentDidMount() {
    // fetch from url = kepanggil sekali diawal, biasa dipake untuk memanggil api atau ambil data
    axios({
      method: 'GET',
      url: 'http://newsapi.org/v2/everything?q=bitcoin&from=2020-09-19&sortBy=publishedAt&apiKey=59263fd7f38d4b81b3a0a1157f59fd8e'
    })
      .then( res => {
        // when success
        console.info('articles', res.data.articles)
        this.setState({listNews: res.data.articles})
      })
      .catch( res => {
        // when error
      });
  }

  render() {
    return (
      <div className="container">
        <div className="text-header"><b>GLINTS NEWS</b></div>
        <div className="news-container">
          {this.state.listNews.map((news, index) => (
            <div className="news-card">

              <div className="news-img">
                <img className="img" src={news.urlToImage} alt="News Image"/>
              </div>

              <div className="news-content" key={index}>
                <div className="title-news">{news.title}</div>
                <div className="title-author"> <div className="icon"></div> {news.author}</div>
                <div className="title-publish"> <div className="icon"></div> {news.publishedAt}</div>
                <div className="title-desc">{news.content}</div>
              </div>
              
            </div>
          ))}
        </div>
      </div>
    )
  }

}