//membuat object promise

fetch('employee.json')
  .then(function (response) {
    return response.json();
  })

  .then(function (data) {
    //console.log(data)
    isiData(data);
  })

  .catch(function (err) {
    console.log('error: ' + err);
  });

function isiData(data) {
  console.log('tes');
  var mainContainer = document.getElementById('myData');

  for (let index = 0; index < data.length; index++) {
    var div = document.createElement('div');
    div.innerHTML = data[index].employee_name + data[index].employee_salary;
    mainContainer.appendChild(div);
  }

}