// May not running without Babel

class Vehicle {
  speed = 0;

  goForward(newSpeed) {
    this.direction = 'Forward';
    this.speed = newSpeed;
  };

  stop() {
    this.speed = 0;
    this.direction = null;
  };
};


class Car extends Vehicle {
  speed = '250 Km/h';
};


class Motorcycle extends Vehicle {
  speed = '125 Km/h';
};


let mobil = new Car;
let motor = new Motorcycle;
console.log(mobil);
console.log(motor);